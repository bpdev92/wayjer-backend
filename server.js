const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./app/routes');
const config = require('./config').server;

const app = express();

app.set('root', `${__dirname}/..`);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

// enable cors in dev
app.use(cors());

// set the base uri
app.set('baseUrl', config.baseUrl);

// mount the routes
app.use(routes);

// mount server
app.listen(config.port, config.host, () => {
  console.log(`app running on http://${config.host}:${config.port}`);
});
