const routes = require('express').Router();

routes.get('/', (req, res) => {
  res.send({msg: 'hello! Server is up and running'});
});

// routes.use('/auth', require('./auth'));
// routes.use('/users', require('./users'));
// routes.use('/projects', require('./projects'));
// routes.use('/tasks', require('./tasks'));

// the catch all route
routes.all('*', (req, res) => {
  res.status(404).send({msg: 'not found'});
});


module.exports = routes;