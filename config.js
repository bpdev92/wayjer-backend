module.exports = {
  server: {
    host: '0.0.0.0',
    port: 3000
  },

  backend: {
    url: 'http://localhost:3000'
  },

  auth: {
    // 180 days in ms
    tokenLifetime: 365 * 24 * 3600 * 1000
  },

  headers: {
    authToken: 'auth-token'
  }
};
